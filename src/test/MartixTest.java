import org.testng.Assert;
import org.testng.annotations.Test;

public class MartixTest {

  @Test
  public void constructor1Test() {
    Matrix M1 = new Matrix(2, 3);
    String expected = "\n0 0 0\n0 0 0\n";
    Assert.assertEquals(M1.toString(), expected);
    Matrix M2 = new Matrix();
    Assert.assertEquals(M2.toString(), "\n");
  }

  @Test
  public void constructor2Test() {
    Matrix M1 = new Matrix(new int[][] { { 1, 2, 3 }, { 2, 5, 6 } });
    String expected = "\n1 2 3\n2 5 6\n";
    Assert.assertEquals(M1.toString(), expected);
    Matrix M2 = new Matrix(new int[][] { { 1, 2 }, { 2, 5 }, { 3, 4 } });
    String expected2 = "\n1 2\n2 5\n3 4\n";
    Assert.assertEquals(M2.toString(), expected2);
  }

  @Test
  public void equalsTest() {
    Matrix M1 = new Matrix(new int[][] { { 1, 2, 3 }, { 2, 5, 6 } });
    Matrix M2 = new Matrix(new int[][] { { 1, 2, 3 }, { 2, 5, 6 } });
    Matrix M3 = new Matrix();
    String M4 = "1 2 3";
    Matrix M5 = new Matrix(new int[][] { { 1, 2, 3 }, { 2, 5, 7 } });
    Matrix M6 = new Matrix(new int[][] { { 1, 2 }, { 2, 5, 7 } });
    Assert.assertTrue(M1.equals(M2));
    Assert.assertFalse(M1.equals(M3));
    Assert.assertFalse(M1.equals(M5));
    Assert.assertFalse(M1.equals(M6));
  }

  @Test
  public void timesTest() {
    Matrix M1 = new Matrix(new int[][] { { 1, 2, 3 }, { 2, 5, 6 } });
    Matrix M2 = new Matrix(new int[][] { { 1, 2, 3 }, { 2, 5, 6 } });
    Matrix M3 = new Matrix(new int[][] { { 1, 2, 3 }, { 2, 5, 6 }, {4, 5, 6} });
    Matrix M4 = new Matrix();
    String expected = "\n17 27 33\n36 59 72\n";
    Assert.assertNull(M1.times(M2));
    Assert.assertEquals(M1.times(M3).toString(), expected);
    Assert.assertNull(M1.times(M4));
  }

  @Test
  public void plusTest() {
    Matrix M1 = new Matrix(new int[][] { { 1, 2, 3 }, { 2, 5, 6 } });
    Matrix M2 = new Matrix(new int[][] { { 1, 2, 3 }, { 2, 5, 6 } });
    Matrix M3 = new Matrix(new int[][] { { 1, 2 }, { 2, 5 }, { 4, 5 } });
    Matrix M4 = new Matrix(new int[][] { { 1, 2, 3 }, { 2, 5, 6 }, {4, 5, 6} });
    Matrix M5 = new Matrix();
    String expected = "\n2 4 6\n4 10 12\n";
    Assert.assertEquals(M1.plus(M2).toString(), expected);
    Assert.assertNull(M1.plus(M3));
    Assert.assertNull(M3.plus(M4));
    Assert.assertNull(M3.plus(M5));
  }

  @Test
  public void transposeTest() {
    Matrix M1 = new Matrix(new int[][] { { 1, 2, 3 }, { 2, 5, 6 } });
    Matrix M2 = new Matrix(new int[][] { { 1, 2 }, { 2, 5 }, { 4, 5 } });
    Matrix M5 = new Matrix();
    String expected = "\n1 2\n2 5\n3 6\n";
    String expected2 = "\n1 2 4\n2 5 5\n";
    String expected3 = "\n";
    Assert.assertEquals(M1.transpose().toString(), expected);
    Assert.assertEquals(M2.transpose().toString(), expected2);
    Assert.assertEquals(M1.transpose().transpose().toString(), M1.toString());
    Assert.assertEquals(M2.transpose().transpose().toString(), M2.toString());
    Assert.assertEquals(M5.transpose().toString(), expected3);
  }

}
